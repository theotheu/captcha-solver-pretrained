# Pretrained models for sci-hub-captcha-solver

This repo is with over 4GB far too big.

It is a copy of the research directory from https://github.com/tensorflow/models.git, but with pretrained models.
The other content, however small, is removed.

# To do
- Remove files that are not required for runtime
- Use tensorflow 2

